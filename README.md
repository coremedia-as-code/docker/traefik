# traefik

Docker Container for an traefik

## Changes between

|         | this | Coremedia |
|:---     | :--- | :---      |
| Version | 2.1  | 1.7       |
| Certificate | `*.cm.local` | `docker.localhost` |
| Makefile    | +            | - |

## Build

Your can use the included Makefile.

To build the Container: `make build`

To remove the builded Docker Image: `make clean`

Starts the Container: `make run`

Starts the Container with Login Shell: `make shell`

Entering the Container: `make exec`

Stop (but **not kill**): `make stop`

