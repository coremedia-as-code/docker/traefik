FROM traefik:v2.1

ENV ENVIRONMENT_FQDN=cm.local

COPY rootfs /

CMD  ["--loglevel=INFO", \
    "--providers.docker=true", \
    "--providers.providersThrottleDuration=5s", \
    "--providers.docker.watch=true", \
    "--providers.docker.exposedbydefault=false", \
    "--accessLog.bufferingSize=0", \
    "--api=true", \
    "--api.dashboard=true", \
    "--api.insecure=true", \
    "--ping.entryPoint=web"]
